<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>登录</title>
		<style type="text/css">
			.login{
				margin: 0 auto;
				height: 400px;
				width: 1200px;
				background-color: #FFFFFF;
			}
			.name{
				border: 1px solid #E6E6E6 ;
				height: 40px;
				width: 300px;
				margin: 15px;
			}
			.text{
				height:35px;
				width: 180px;
				float: right;
				border: none;
				outline: none;
			}
			.name2{
				float: left;
				height: 40px;
				width: 110px;
				border-right:1px solid #E6E6E6 ;
				background-color: #FBFBFB;
				font-size: 15px;
				text-align: center;
				line-height: 40px;
			}
			.main_top{
				margin: 0 0 0 17px;
			}
			.main_top1{
				font-size: 13px;
				width: 95px;
				height: 40px;
				float: left;
				text-align: center;
				line-height: 40px;
			}
			#mt{
				border-bottom: 2px solid #009688;
				color: #009688;
			}
			.register{
				
				width: 92px;
				height: 40px;
				background-color: #009688;
				margin: 15px;
				font-size: 15px;
				text-align: center;
				line-height: 40px;
				float: left;
			}
			.register1{
				
				width: 92px;
				height: 40px;
				margin: 15px;
				font-size: 15px;
				text-align: center;
				line-height: 40px;
				float: left;
			}
			.main_btm{
				font-size: 13px;
				margin: 15px;
			}
			.main_btm img{
				position: relative;top: 5px;
			}
			.eyes{
				
				float: right;
				font-size: 15px;
				color: darkgrey;
				position: relative;
				top: -27px;
				right: 5px;
			}
			.name_wrong{
				width:300px;
			height: 40px;
			border: 1px solid red;
			float: right;
			position: relative;
			top: -57px;
			left: -580px;
			}
			/*.password_wrong{
				
				width:300px;
			height: 40px;
			border: 1px solid red;
			float: right;
			position: relative;
			top: -58px;
			left: -580px;
			}*/
			.yzm_wrong{
				width:300px;
			height: 66px;
			border: 1px solid red;
			float: right;
			position: relative;
			top: -58px;
			left: -408px;
			}
			.yzm{
			display: inline-block;
			border: 1px solid red;
			float: right;
			position: relative;
			top: -50px;
			left: -795px;
			}
			.Login_top{
			
			margin-bottom:20px;
			
			}
			.Login_bottom{			
			
			position:relative;
			top:200px;			
			}
		</style>
		<script type="text/javascript" src="js/login.js"></script>
		<link rel="stylesheet" href="css/fontawesome/font-awesome-4.7.0/css/font-awesome.css" />
	</head>
	<body style="background-color: #F2F2F2;">
		<div class="Login_top">
		<c:import url="top.jsp"></c:import>
		</div>
		
		<form action="<!--登录的servlet：查询数据库-->">
			<div class="login">
				<div class="main_top">
					<div class="main_top1" id="mt">登入</div>
					<div class="main_top1" ><a href="Register.html" style="font-size: 13px;text-decoration: none;color: black;">注册</a></div>
				</div>
				<hr style="width: 1165px ;border: 0.5px solid #E6E6E6;"/>
				<div class="name">
					<div class="name2">用户名</div>
					<div class="name1">
						<input type="text" placeholder="请使用用户名/邮箱登录" class="text" />
					</div>
				</div>
				<div class="name_wrong" id=""></div>
				<div class="name">
					<div class="name2">密码</div>
					<div class="name1">
						<input type="password"  class="text pwd" />
					</div>
					<div class="eyes" onclick="openEye(this)">
						<i class="fa fa-eye" aria-hidden="true" style="display: none;"></i>
						
					</div>
					<div class="eyes" onclick="openEye(this)">
						<i class="fa fa-eye-slash" aria-hidden="true" ></i>
					</div>
				</div>
				<div class="password_wrong" id=""></div>
				<div class="name">
					<div class="name2">验证码</div>
					<div class="name1">
						<input type="text" placeholder="不区分大小写" class="text" />
					</div>
				</div>
				<span class="yzm">验证码错误</span>
				<div class="yzm_wrong" id=""></div>
				<div class="register">
					<input type="submit" value="立即登入" style="border:none;background:none;cursor:pointer ;outline: none; color: #FFFFFF;"/>
				</div>
				<div class="register1">
					<input type="checkbox" />&nbsp;<a href="" style="text-decoration: none;color: #999999;">自动登录</a>
					<!--<input type="button" value="忘记密码？" style="border:none;background:none;cursor:pointer;color: black;outline: none;"/>-->
				</div>
				<div class="main_btm" style="clear: both;">
					或者直接使用社交账号快捷注册 &nbsp;
					<img src="img/2018-07-20_134416.gif" />
				</div>
			</div>
				<div class="Login_bottom">
		<c:import url="bottom.jsp"></c:import>
		</div>
		</form>
	</body>
</html>
