<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta  charset="utf-8">
		<title>上部</title>
		<link href="css/top.css" rel="stylesheet" type="text/css">
	</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

	<div class="top-box-box">
		<div class="top-box">
			<div class="top-left">
				<a href="main.jsp" class="top-a">
					<img src="img/topimg3.jpg" alt="" class="top_img" />
				</a>
				<span class="top-left-span">只有学不到的知识，没有做不到的业务</span>
			</div>
			<div class="top-right" >
				<ul class="top-ul-right">
					<li><img src="img/top6.jpg" alt="" /></li>
					<li><img src="img/top5.jpg" alt="" /></li>
					<li><span onclick="toReg()">注册</span></li>
					<li><span onclick="toLogin()">登录</span></li>
					<li><img src="img/top4.jpg" alt="" onclick="toLogin()" /></li>
				</ul>
			</div>
			<div class="top-right">
				<ul class="top-ul-right" style="display:none;">
					<li class="top-out"><a href="#">退出</a></li>
					<li onclick="toUser()"><img src="img/top1.jpg" alt="" / class="head"></li>
					<li><span>某某</span></li>
				</ul>
			</div>
		</div>
	</div>
	
</body>
<script type="text/javascript" src="js/top.js"></script>
</html>
