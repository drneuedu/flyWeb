<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
		
		<title></title>
		<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
    <script type="text/javascript" charset="utf-8" src="utf8-jsp/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="utf8-jsp/ueditor.all.min.js"> </script>
	 <script type="text/javascript" charset="utf-8" src="utf8-jsp/lang/zh-cn/zh-cn.js"></script>
	<script type="text/javascript">
	var ue = UE.getEditor('editor');   
	</script>
		
		
		<!--CSS-->
		<style type="text/css">
			*{
				padding: 0px;
				margin: 0px;
				list-style: none;
				text-decoration: none;
			}
			body{
				background-color: #F2F2F2;
			}
			.c1{
				background-color: white;
				border: 1px solid #E6E6E6;
				width: 1170px;
				margin: 0px auto;
				position:relative;
				top:20px;
			}
			.c2{
				border-bottom: 3px solid #5FB878;
				height: 60px;
				width: 85px;
				text-align: center;
				line-height: 60px;
				color: #009688;
				position: relative;
				left: 20px;
			}
			.c2:hover{
				cursor: pointer;
			}
			hr{
				width: 1129px;
				margin: 0px auto;
			}
			.c3{
				/*border: 1px solid black;*/
				height: 85px;
				width: 1129px;
				margin: 0px auto;
			}
			.c4{
				/*border: 1px solid #E6E6E6;*/
				height: 42px;
				width: 252px;
				float: left;
				position: relative;		
				top: 20px;
			}
			.c5{
				border: 1px solid #E6E6E6;
				background-color: #FBFBFB;
				height: 40px;
				width: 110px;
				float: left;
				text-align: center;
				line-height: 42px;
				font-size: 13px;
			}
			.c6{
				border: 1px solid #E6E6E6;
				height: 42px;
				width: 140px;
				font-size: 13px;
				text-align: center;
				line-height: 40px;
				float: left;	
			}
			.c7{
				/*border: 1px solid #E6E6E6;*/
				height: 42px;
				width: 430px;
				float: left;
				position: relative;		
				top: 20px;
				left: 30px;
			}
			.c8{
				border: 1px solid #E6E6E6;
				background-color: #FBFBFB;
				height: 40px;
				width: 110px;
				float: left;				
				text-align: center;
				line-height: 42px;
				font-size: 13px;
			}
			.c9{
				border: 1px solid #E6E6E6;
				height: 40px;
				width: 316px;
				float: left;					
			}
			.c10{
				border: none;
				outline: none;
				height: 38px;
				width: 295px;
				float: left;
				position: relative;		
				left: 10px;
			}
			.c11{
				/*border: 1px solid #E6E6E6;*/
				height: 42px;
				width: 350px;
				float: left;
				position: relative;		
				top: 20px;
				left: 60px;
			}
			.c12{
				border: 1px solid #E6E6E6;
				background-color: #FBFBFB;
				height: 40px;
				width: 110px;
				float: left;				
				text-align: center;
				line-height: 42px;
				font-size: 13px;
			}
			.c13{
				border: 1px solid #E6E6E6;
				height: 42px;
				width: 140px;
				font-size: 13px;
				text-align: center;
				line-height: 40px;
				float: left;
			}
			.c14{
				border: 1px solid #E6E6E6;
				/*height: 100px;*/
				width: 1129px;
				margin: 0px auto;
			}
			.c15{
				/*border: 1px solid #E6E6E6;*/
				height: 80px;
				width: 100px;
				position: relative;
				left: 20px;
			}
			.c16{
				border: 1px solid #009688;
				background-color: #009688;
				height: 40px;
				width: 92px;
				color: white;
				font-size: 15px;
				position: relative;
				top: 20px;				
			}
			.c16:hover{
				cursor: pointer;
			}
			
			.yin_bottom{position:relative;top:100px;}
		</style>
		
		
	</head>
	<body>
	
		<form action="<!--发帖的servlet：向数据库帖子信息表添加数据-->">
		<div class="yin_top">
		<c:import url="top.jsp"></c:import>
		</div>
		<!--top区域的网页-->
		<div class="top">
					
		</div>
		
		<!--整个工作区域-->
		<div class="c1">
			
			<!--发表新帖标题-->
			<div class="c2">
				发表新帖
			</div>
			
			<!--横线-->
			<hr />
			
			<!--三个专栏的边框-->
			<div class="c3">
				 
				<!--所在专栏-->
				<div class="c4">
					<div class="c5">所在专栏</div>
				    <select class="c6" name="专栏">
					    <option>请选择</option>
					    <option>JAVA</option>
					    <option>HTML5</option>
					    <option>Oracle</option>
					    <option>AJAX</option>
				    </select>
				</div>				
				    
				<!--标题栏-->
				<div class="c7">
					<div class="c8">标题</div>
			        <div class="c9">
			        	<input type="text" class="c10" name="标题" value=""/>
			        </div>
				</div>
				
				<!--文章状态-->
				<div class="c11">
					<div class="c12">请选择文章状态</div> 
				    <select class="c13">
				        <option >请选择</option>
				    	<option >分享</option>
				    	<option >提问</option>
				    	<option >讨论</option>
				    	<option >建议</option>
				    </select>
				</div>
			</div>
			
			<!--富文本框整体边框-->
			<div class="c14">
				
			<script id="editor" type="text/plain" style="width:1129px;"></script>	
				
				
				
			</div>
			
			<!--发布按钮-->
			<div class="c15">
				<input type="submit" value="立即发布" class="c16" />
			</div>
			
		</div>
		
			
		<!--bottom区域-->
		<div class="bottom">
		        	
		</div>
		<div class="yin_bottom">
		<c:import url="bottom.jsp"></c:import>
		</div>
		</form>	
	</body>
</html>
    