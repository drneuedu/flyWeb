<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>个人中心</title>
		<link href="css/left.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="css/fontawesome/font-awesome-4.7.0/css/font-awesome.min.css" />
	</head>
	<body >
	<c:import url="top.jsp"></c:import>
		<div class="m">
			<div class="main">
				<div class="main-left">
					<ol class="main-left-box">
						<li onclick="show(this)" value="0" class="i1">
							<i class="fa fa-user-circle" aria-hidden="true"></i>
							用户主页
						</li>
						<li onclick="show(this)" value="1">
							<i class="fa fa-cog" aria-hidden="true"></i>
							基本设置
					 	</li>
						<li onclick="show(this)" value="2">
							<i class="fa fa-file" aria-hidden="true"></i>
							我的帖子
						</li>
					</ol>
				</div>
				<div class="main-main" >
					<div class="main-main-top">
						<div class="main-main-top-head">
							<img src="img/12.jpg" class="main-main-top-img"/>
						</div>
						<div class="main-main-top-name">
							<span>某某</span>
						</div>
						<div class="main-main-top-city">
							<i class="fa fa-map-marker" aria-hidden="true"></i>
							<span>来自城市</span>
						</div>
						<div class="main-main-top-sign">
							<span>这里是个性签名</span>
						</div>
					</div>
					<div class="main-main-left">
						<div class="main-main-left-top">
							<span>某某</span>最近的的提问
						</div>
						<div class="main-main-left-text">
							<ol>
								<li>提问1</li>
								<li>提问2</li>
							</ol>
						</div>
					</div>
					<div class="main-main-left">
						<div class="main-main-left-top">
							<span>某某</span>最近的的回复
						</div>
						<div class="main-main-left-text">
							<ol>
								<li></li>
							</ol>
						</div>
					</div>
				</div>
				<div class="main-main"   style="display: none;" >
					<div class="main-main2-top">
						<ol>
							<li onclick="show1(this)" value="0" class="main-main2-top-l1">
								<i class="fa fa-cog" aria-hidden="true"></i>
								修改资料
							</li>
							<li onclick="show1(this)" value="1" class="main-main2-top-l2">
								<i class="fa fa-cog" aria-hidden="true"></i>
								修改密码
							</li>
						</ol>
					</div>
					<div class="main-main2-text" >
						<form action="" method="post">
							<ol class="main-main2-ol1">
								<li>
									<div class="main-main2-ol1-text1">用户名</div>
									<div class="main-main2-ol1-text2">
										<input type="text" name="username" id="username" value="" />
									</div>
									<div class="main-main2-ol1-text3">
										
									</div>
								</li>
								<li>
									<div class="main-main2-ol1-text1">昵称</div>
									<div class="main-main2-ol1-text2">
										<input type="text" name="username" id="username" value="" />
									</div>
									<div class="main-main2-ol1-text3">
										
									</div>
								</li>
								<li>
									<div class="main-main2-ol1-text1">验证码</div>
									<div class="main-main2-ol1-text2">
										<input type="text" name="username" id="username" value="" />
									</div>
									<div class="main-main2-ol1-text3">
										
									</div>
								</li>
								<li>
									<div class="main-main2-ol1-text1">邮箱</div>
									<div class="main-main2-ol1-text2">
										<input type="text" name="username" id="username" value="" />
									</div>
									<div class="main-main2-ol1-text3">
										
									</div>
								</li>
								
								<li>
									<div class="main-main2-ol1-text1">
										城市
									</div>
									<div class="main-main2-ol1-text2">
										<input type="text" name="username" id="username" value="" />
									</div>
									<div class="main-main2-ol1-text3">
										
									</div>
								</li>
								<li>
									<div class="main-main2-ol1-sex">
										<input type="radio" value="男" name="sex"/><span>男</span>
										<input type="radio" value="女" name="sex"/><span>女</span>
									</div>
								</li>
							</ol>
							<ol class="main-main2-ol2">
								<li class="main-main2-ol2-li1">
									<img src="img/12.jpg" />
								</li>
								<li class="main-main2-ol2-li2">
									<input type="file" name="" id="" value="上传头像" />
								</li>
							</ol>
							<ol class="main-main2-ol3">
								<li class="main-main2-ol3-li1">
									签名
								</li>
								<li class="main-main2-ol3-li2">
									
								</li>
							</ol>
							<input type="submit" class="submit" value="确认修改"/>
						</form>
					</div>
					<div class="main-main2-text main-main2-pwd" style="display: none;">
						<form action="" method="post">
							<ol class="main-main2-ol1">
								<li>
									<div class="main-main2-ol1-text1">原密码</div>
									<div class="main-main2-ol1-text2">
										<input type="text" name="username" id="username" value="" />
									</div>
									<div class="main-main2-ol1-text3">
										
									</div>
								</li>
								<li>
									<div class="main-main2-ol1-text1">新密码</div>
									<div class="main-main2-ol1-text2">
										<input type="text" name="username" id="username" value="" />
									</div>
									<div class="main-main2-ol1-text3">
										
									</div>
								</li>
								<li>
									<div class="main-main2-ol1-text1">确认密码</div>
									<div class="main-main2-ol1-text2">
										<input type="text" name="username" id="username" value="" />
									</div>
									<div class="main-main2-ol1-text3">
										
									</div>
								</li>
								
							<input type="submit" class="submit" value="确认修改"/>
						</form>
					</div>
				</div>
				<div class="main-main" style="display: none;" >
					<div class="main-main2-top">
						<ol>
							<li>
								<i class="fa fa-file-text" aria-hidden="true"></i>
								我发的帖子
							</li>
						</ol>
					</div>
					<div class="main-main3-text">
						<table border="1px" cellspacing="0" cellpadding="0">
							<tr class="tahead">
								<td class="td0"></td>
								<td class="td1">帖子标题</td>
								<td class="td2">专栏</td>
								<td class="td3">发表时间</td>
								<td class="td2">状态</td>
								<td class="td2">操作</td>
							</tr>
							<tr class="tabody">
								<td class="td0"><input type="checkbox" name="checkbox" value="" class="tabody-td0-in"></td>
								<td class="td1">帖子标题</td>
								<td class="td2">专栏</td>
								<td class="td3">2018.04.25</td>
								<td class="td2">建议</td>
								<td class="td2"><a href="" class="main-main3-tabody-text">编辑</a></td>
							</tr>
						</table>
						<table border="0" cellspacing="0" cellpadding="0" style="margin-top: 10px;">
							<tr class="tbfloot">
								<td style="width: 50px;"></td>
								<td class="td0"><input type="checkbox" name="checkbox" value="" onclick="selectAllNullorReserve(this)">全选</td>
								<td class="td1">
									<span>>></span>
            							 <a href=" ">1</a>
            							 <a href=" ">2</a>
            							 <a href=" ">3</a>
            						<span><<</span>
           						</td>	 
								<td class="td2"><a href="" class="main-main3-tabody-text">删除</a></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>
		<c:import url="bottom.jsp"></c:import>
	</body>
	<script type="text/javascript" src="js/user.js"></script>
</html>
