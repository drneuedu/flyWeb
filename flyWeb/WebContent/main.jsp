<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>主页</title>
		<link rel="stylesheet" href="css/main.css" />
		<link rel="stylesheet" href="css/fontawesome/font-awesome-4.7.0/css/font-awesome.css" />
		<script type="text/javascript" src="js/main.js"></script>	
	</head>
	<body onscroll="showBackTop()">
		<div class="yin_top">
		<c:import url="top.jsp"></c:import>
		</div>
		
		<div class="main_d1">
		<div class="main_content" style="width:1435px;">
			<div class="abox">
			<a href="mainlServlet?ptype=首页" class="d1_a" name="homepage">首页</a>
			<a href="mainlServlet?ptype=java" class="d1_a" name="java">JAVA</a>
			<a href="mainlServlet?ptype=html5" class="d1_a" name="html5">HTML5</a>
			<a href="mainlServlet?ptype=oracle" class="d1_a" name="oracle">Oracle</a>
			<a href="mainlServlet?ptype=ajax" class="d1_a"  name="ajax">AJAX</a>
			</div>
			<button class="d1_but" onclick="toFt()">发表帖子</button>
			
			<div class="search_text">
			<input type="text" class="d1_search" placeholder="search"/>
			<div class="search_img" onclick="searchMess()">
				<i class="fa fa-search" id="fa-search" aria-hidden="true"></i>
			</div>
			</div>
			</div>
		</div>
		<div class="main_d2 ">
			<div class="main_lb">
				
				
				<img src="img/lunbo1.jpg" class="lb_img" id="lb_img"/>
				
				
					<a href="" class="lb_a">1</a>
					<a href="" class="lb_a">2</a>
					<a href="" class="lb_a">3</a>
					
			</div>
			
			<div class="main_hottalk"><!--最多放10条-->
				<div class="ht_top">
					<span class="ht_text">本周热点</span>
				</div>
				<ol class="ht_ol">
				<c:forEach items="${hotList }" var="hot">
					<li class="ht_li">
					<a href="" class="ht_a">${hot.title }</a>
					<i class="fa fa-commenting-o" aria-hidden="true"id="ht_comment"></i>
					<span class="ht_span">${hot.commentnum }</span>
					</li>
					</c:forEach>
				
				</ol>
			</div>
			<div class="main_message">
				<div class="mess_top">
					<a href="mainlServlet?ptype=首页" class="mess_a" >综合</a>
					<a href="mainjServlet?state=分享" class="mess_a" >分享</a>
					<a href="mainjServlet?state=提问" class="mess_a" >提问</a>
					<a href="mainjServlet?state=讨论" class="mess_a" >讨论</a>
					<a href="mainjServlet?state=建议" class="mess_a" >建议</a>
					
				</div>
				<c:forEach items="${postList }" var="post">
				<div class="mess_content">
					<img src="180719/img/12.jpg" class="con_img" onclick="toTUser()"/>
					<a href="<!--根据帖子标题跳转到查询帖子信息的数据库-->" class="con_messtext">${post.title }</a>
					<a href="<!--根据昵称跳转到个人信息的数据库-->" class="con_messname">${post.user.username }</a>
					<span class="con_messtime">${post.ptime }</span>
					<div class="con_comment">
					
					<i class="fa fa-commenting-o" aria-hidden="true"id="con_comment">  ${post.commentnum }</i>
					<i class="fa fa-thumbs-o-up" aria-hidden="true" id="con_nice"><span>  ${post.nice}</span></i>
					<div class="con_jing"><span>精贴</span></div>
					
						
					</div>	
					
				</div>
				</c:forEach>
				
				
				
				
				<div class="mess_page">
					<div class="mess_pagecover">
						
					
					<i class="fa fa-chevron-left" aria-hidden="true" id="mess_pageNuml"></i>
					<a href="" class="mess_pageNum" onclick="changColor(this)">
						1
					</a>
					<a href="" class="mess_pageNum pn" onclick="changColor(this)">
						1
					</a>
					<a href="" class="mess_pageNum pn" onclick="changColor(this)">
						1
					</a>
					<a href="" class="mess_pageNum pn" onclick="changColor(this)">
						1
					</a>
					<a href="" class="mess_pageNum pn" onclick="changColor(this)">
						1
					</a>
					<a href="" class="mess_pageNum pn" onclick="changColor(this)">
						1
					</a>
				
				
					<i class="fa fa-chevron-right" aria-hidden="true" id="mess_pageNumr"></i> 
					</div>
				</div>
				
				
				
				
				
				
			</div>
			<div class="main_vido" >
				<video class="backmusic"  controls="controls" autoplay="" >
					<source src="C:/Users/Administrator/Desktop/禾几论坛/视频" type="video/mp4" />
				</video>
				
			</div>
			<div class="main_big">
				
				<div class="big_top">
					<span class="big_text">大牛榜</span>
				</div>
				<table class="big_table" cellpadding="0"cellspacing="0">
					<tr class="big_tr">
						<td class="big_td">
						<img src="180719/img/12.jpg" class="big_img" onclick="toTUser()"/>
						<a href="<!--跳转到对应用户的个人主页-->" class="big_td_a">简默大爷</a>
						</td>
						<td class="big_td">2</td>
						<td class="big_td">3</td>
						<td class="big_td">4</td>
					</tr>
					<tr class="big_tr">
						<td class="big_td">5</td>
						<td class="big_td">6</td>
						<td class="big_td">7</td>
						<td class="big_td">8</td>
					</tr>
					<tr class="big_tr">
						<td class="big_td">9</td>
						<td class="big_td">10</td>
						<td class="big_td">11</td>
						<td class="big_td">12</td>
					</tr>
					<tr class="big_tr">
						<td class="big_td">13</td>
						<td class="big_td">14</td>
						<td class="big_td">15</td>
						<td class="big_td">16</td>
					</tr>
					<tr class="big_tr">
						<td class="big_td">17</td>
						<td class="big_td">18</td>
						<td class="big_td">19</td>
						<td class="big_td">20</td>
					</tr>
				</table>
			
				<p class="big_line"></p>
			<div class="main_line">
				<div class="line_top">
					<span class="line_text">友情链接</span>
				</div>
				<table class="line_table" cellpadding="0" cellspacing="0">
					<tr class="line_tr">
						<td class="line_td">
							<a href="https://www.csdn.net/"target="_blank">CSDN</a>
						</td>
						<td class="line_td">
							<a href="https://www.cnblogs.com/"target="_blank">博客园</a>
						</td>
						<td class="line_td">
							<a href="https://www.w3cschool.cn/"target="_blank">W3Cschool</a>
						</td>
						<td class="line_td">
							<a href="https://gitee.com/"target="_blank">码云</a>
						</td>
					</tr>
					<tr class="line_tr">
						<td class="line_td">
							<a href="https://github.com/"target="_blank">GitHub</a>
						</td>
						<td class="line_td">
							<a href="https://www.linuxidc.com/"target="_blank">Linux公社</a>
						</td>
						<td class="line_td">
							<a href="https://www.ithome.com/"target="_blank">IT之家</a>
						</td>
						<td class="line_td">
							<a href="https://www.gn00.com/"target="_blank">技术宅社区</a>
						</td>
					</tr>
				</table>
			</div>
			
			</div>
			
		</div>
		<div class="main_quickwrite " onclick="toFt()">
			<i class="fa fa-pencil" aria-hidden="true"id="quick_pen"></i>
		</div>
		<div class="main_backtop " onclick="doLoad()"><!--拉动滚动条显示该div-->
			<i class="fa fa-arrow-up" aria-hidden="true" id="back_top"></i>
		
		</div>
		<div class="yin_bottom">
		<c:import url="bottom.jsp"></c:import>
		</div>
	</body>
</html>

