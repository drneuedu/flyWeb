<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
		<!--<meta charset="utf-8" />-->
		<title>帖子详情&回帖</title>
		<link rel="stylesheet" href="css/style.css" />
		<link rel="stylesheet" type="text/css" href="css/fontawesome/font-awesome-4.7.0/css/font-awesome.css"/>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
    <script type="text/javascript" charset="utf-8" src="utf8-jsp/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="utf8-jsp/ueditor.all.min.js"> </script>
	 <script type="text/javascript" charset="utf-8" src="utf8-jsp/lang/zh-cn/zh-cn.js"></script>
	<script type="text/javascript">
	var ue = UE.getEditor('editor');   
	</script>
	
	</head>
	<body>
		<div class="yin_top">
		<c:import url="top.jsp"></c:import>
		</div>
		<div id="frame">
		    
		    <div id="post">
		    	<div class="pitch">
		    		<div class="firstfloor">
		    			<h2>贴子名称:别超20个字</h2>
		    			<div class="module1">
		    				<i class="fa fa-commenting-o" aria-hidden="true"><span>  53</span></i>
		    			</div>
		    			<div class="module2">
		    				<i class="fa fa-eye" aria-hidden="true"><span>  5648</span></i>
		    			</div>
		    			<div class="module3"><span>精贴</span></div>
		    		</div>
		    		<div class="secondfloor">
		    			<div class="photo"><a href=""><img src="img/2018-07-20_162144.gif"/></a></div>
		    			<div class="username"><a href="">芥末小s</a></div>
		    			<div class="dateline"><span>2018-07-05</span></div>
		    		</div>
		    		<div class="thirdfloor">
		    			<textarea>问题内容，用文本域写的</textarea>
		    		</div>
		    	</div>
		    </div>
		    
		    <div id="reply">
		    	<div class="pitch">
		    		<div class="firstfloor">
		    				<div class="left"></div>
		    				<div class="span"><span>回帖</span></div>
		    				<div class="right"></div>
		    		</div>
		    		<div class="secondfloor">
		    			<div class="num1">
		    				<div class="photo"><a href=""><img src="img/2018-07-20_173126.gif"/></a></div>
		    			    <div class="username"><a href="">岁月小偷</a></div>
		    			    <div class="dateline"><span>2018-07-06</span></div>
		    			</div>
		    			<div class="num2">
		    				<textarea>评论内容，用文本域写的</textarea>
		    			</div>
		    			<div class="num3">
		    				<div class="num3_1">
		    					<i class="fa fa-thumbs-o-up" aria-hidden="true"><span>     0</span></i>
		    				</div>
		    				<div class="num3_2">
		    					<i class="fa fa-commenting-o" aria-hidden="true"><a href="">  回复</a></i>
		    				</div>
		    			</div>
		    			
		    				
		    				<!--<button class="pushcomment-but"/>发表评论</button>-->
		    			
		    		</div>
		    		
		    		<div class="thirdfloor">
		    			<script id="editor" type="text/plain" style="width:740px;">
		    				
		    			</script>
		    		</div>
		    		<input type="submit" value="提交回复" />
		    	</div>
		    </div>
		</div>
		<div class="yin_bottom">
		<c:import url="bottom.jsp"></c:import>
		</div>
	</body>
</html>
