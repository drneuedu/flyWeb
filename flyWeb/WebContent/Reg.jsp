<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>注册</title>
		<style type="text/css">
			
			.main{
				margin: 0 auto;
				height: 550px;
				width: 1200px;
				background-color: #FFFFFF;
			}
			.name{
				border: 1px solid #E6E6E6 ;
				height: 40px;
				width: 300px;
				margin: 15px;
				
			}
			.text{
				height:35px;
				width: 180px;
				float: right;
				border: none;
				outline: none;
			}
			.name2{
				float: left;
				height: 40px;
				width: 110px;
				border-right:1px solid #E6E6E6 ;
				background-color: #FBFBFB;
				font-size: 15px;
				text-align: center;
				line-height: 40px;
			}
			.checkbox{
				height: 40px;
				width: 300px;
				margin: 15px;
				margin-bottom: -20px;
			}
			.checkbox a{
				font-size: 13px;
				color: #999999;
				text-decoration: none;
			}
			.main .register{
				width: 92px;
				height: 40px;
				background-color: #009688;
				margin: 15px;
				font-size: 15px;
				text-align: center;
				line-height: 40px;
			}
			.main .register {
				text-decoration: none;
				color: #FFFFFF;
			}
			
			.main_btm{
				font-size: 13px;
				margin: 15px;
			}
			.main_btm img{
				position: relative;top: 5px;
			}
			.main_top{
				margin: 0 0 0 17px;
			}
			.main_top1{
				font-size: 13px;
				width: 95px;
				height: 40px;
				float: left;
				text-align: center;
				line-height: 40px;
			}
			#mt{
				border-bottom: 2px solid #009688;
				color: #009688;
			}
			.eyes{
				
				float: right;
				font-size: 15px;
				color: darkgrey;
				position: relative;
				top: -27px;
				right: 5px;
			}
			.name_wrong{width:300px;
			height: 40px;
			
			float: right;
			position: relative;
			left: -680px;
			top:-57px;
			}
			.password_wrong{
				
				width:300px;
			height: 40px;
			float: right;
			position: relative;
			left: -680px;
			top:-57px;
			}
			.mail_wrong{
				
				width:300px;
			height: 40px;
			float: right;
			position: relative;
			left: -680px;
			top:-57px;
			}
			.Reg_top{
			
			margin-bottom:20px;
			
			}
			
		</style>
		<link rel="stylesheet" href="css/fontawesome/font-awesome-4.7.0/css/font-awesome.css" />
		<script type="text/javascript" src="js/Reg.js"></script>
	</head>
	<body style="background-color: #F2F2F2;">
		<form action="<!--注册的servlet：向数据库中用户信息表添加数据-->">
			<div class="Reg_top">
		<c:import url="top.jsp"></c:import>
		</div>
			
			<div class="all">
			<!--main-->
			<div class="main">
				<div class="main_top">
					<div class="main_top1" ><a href="Login.html" style="font-size: 13px;text-decoration: none;color: black;" >登录</a> </div>
					<div class="main_top1"id="mt">注册</div>
				</div>
				<hr style="width: 1165px ;border: 0.5px solid #E6E6E6;"/>
				<div class="name">
					<div class="name2">用户名</div>
					<div class="name1">
						<input type="text" class="text" />
					</div>
				</div>
				
				<div class="name_wrong" id=""></div>
				<div class="name">
					<div class="name2">真实姓名</div>
					<div class="name1">
						<input type="text" class="text" />
					</div>
				</div>
				<div class="name">
					<div class="name2">密码</div>
					<div class="name1">
						<input type="password" placeholder="6到16个字符" class="text pwd" />
					</div>
				
				
				
				
				
				</div>
				<div class="password_wrong" id=""></div>
				<div class="name">
					<div class="name2">确认密码</div>
					<div class="name1">
						<input type="password"  class="text qpwd" />
					</div>
					
					<div class="eyes" onclick="openEye(this)">
						<i class="fa fa-eye" aria-hidden="true" style="display: none;"></i>
						
					</div>
					<div class="eyes" onclick="openEye(this)">
						<i class="fa fa-eye-slash" aria-hidden="true" ></i>
					</div>
					
					
				</div>
				
				<div class="name">
					<div class="name2">邮箱</div>
					<div class="name1">
						<input type="text" class="text" />
					</div>
				</div>
				<div class="mail_wrong" id=""></div>
				<div class="name">
					<div class="name2">城市</div>
					<div class="name1">
						<input type="text" class="text" />
					</div>
				</div>
				<div class="checkbox">
					<input type="checkbox" />&nbsp;<a href="">同意注册协议</a>
				</div>
				<div class="register">
					<input type="submit" value="立即注册" style="border:none;background:none;cursor:pointer;outline:none ; color: #FFFFFF;"/>
				</div>
				<div class="main_btm">
					或者直接使用社交账号快捷注册 &nbsp;
					<img src="img/2018-07-20_134416.gif" />
				</div>
			</div>
			
		</div><br /><br /><br /><br /><br />
		<hr style="border: 0.5px solid #E6E6E6;"/>
		<c:import url="bottom.jsp"></c:import>
		</form>
	</body>
</html>
