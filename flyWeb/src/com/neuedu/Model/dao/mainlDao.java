package com.neuedu.Model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.neuedu.Model.entity.Post;
import com.neuedu.Model.entity.User;
import com.neuedu.Util.DBUtil;

public class mainlDao {
//点击首页查询所有文章
	public List selectAllPost(){
		Connection conn=DBUtil.getConn();
		List<Post> list=new ArrayList<Post>();
		String sql="select headname,title,username,ptime,nice,commentnum "
				+ " from hj_post hj,hj_user hu "
				+ " where hj.userid=hu.userid";
		try {
			PreparedStatement ps=conn.prepareStatement(sql);
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				Post post=new Post();
				User user=new User();
				user.setHeadname(rs.getString("headname"));
				user.setUsername(rs.getString("username"));
				post.setUser(user);
				post.setTitle(rs.getString("title"));
				post.setPtime(rs.getDate("ptime"));
				post.setNice(rs.getInt("nice"));
				post.setCommentnum(rs.getInt("commentnum"));
				list.add(post);
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return list;
	
	}
//根据专栏分类
	public List selectPtypePost(String ptype) {
		Connection conn=DBUtil.getConn();
		List<Post> list=new ArrayList<Post>();
		String sql="select headname,title,username,ptime,nice,commentnum "
				+ " from hj_post hj,hj_user hu "
				+ " where hj.userid=hu.userid "
				+ " and ptype=?";
		try {
			PreparedStatement ps=conn.prepareStatement(sql);
			ps.setString(1, ptype);
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				Post post=new Post();
				User user=new User();
				user.setHeadname(rs.getString("headname"));
				user.setUsername(rs.getString("username"));
				post.setUser(user);
				post.setTitle(rs.getString("title"));
				post.setPtime(rs.getDate("ptime"));
				post.setNice(rs.getInt("nice"));
				post.setCommentnum(rs.getInt("commentnum"));
				list.add(post);
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		finally{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		
		
		return list;
	}
//查询本周热议
	public List selectTitle() {
		Connection conn=DBUtil.getConn();
		List<Post> list=new ArrayList<Post>();
		String sql="select title,commentnum from hj_post "
				+ " where postid<=10"
				+ " order by title desc";
		try {
			PreparedStatement ps=conn.prepareStatement(sql);
			ResultSet rs=ps.executeQuery();
			while(rs.next()){
				Post post=new Post();
				post.setTitle(rs.getString("title"));
				post.setCommentnum(rs.getInt("commentnum"));
				list.add(post);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally{
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return list;
	}

	
	
}
