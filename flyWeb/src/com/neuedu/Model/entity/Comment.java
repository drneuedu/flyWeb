package com.neuedu.Model.entity;

import java.util.Date;

public class Comment {

	
	private String discuss;
	private Date ctime;
	private int cnice;
	private int replynum;
	private Post post;
	private User user;
	
	
	public Comment() {
		super();
	}


	public Comment(String discuss, Date ctime, int cnice, int replynum, Post post, User user) {
		super();
		this.discuss = discuss;
		this.ctime = ctime;
		this.cnice = cnice;
		this.replynum = replynum;
		this.post = post;
		this.user = user;
	}


	public String getDiscuss() {
		return discuss;
	}


	public void setDiscuss(String discuss) {
		this.discuss = discuss;
	}


	public Date getCtime() {
		return ctime;
	}


	public void setCtime(Date ctime) {
		this.ctime = ctime;
	}


	public int getCnice() {
		return cnice;
	}


	public void setCnice(int cnice) {
		this.cnice = cnice;
	}


	public int getReplynum() {
		return replynum;
	}


	public void setReplynum(int replynum) {
		this.replynum = replynum;
	}


	public Post getPost() {
		return post;
	}


	public void setPost(Post post) {
		this.post = post;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	@Override
	public String toString() {
		return "Comment [discuss=" + discuss + ", ctime=" + ctime + ", cnice=" + cnice + ", replynum=" + replynum
				+ ", post=" + post + ", user=" + user + "]";
	}
	
	
	
}
