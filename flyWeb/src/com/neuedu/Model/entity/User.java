package com.neuedu.Model.entity;

public class User {

	private int userid ;
	private String username;
	private String password;
	private String realname;
	private String sex;
	private String mail;
	private String city;
	private String headname;
	private String signature;
	
	
	public User() {
		super();
	}

	public User(int userid, String username, String password, String realname, String sex, String mail, String city,
			String headname, String signature) {
		super();
		this.userid = userid;
		this.username = username;
		this.password = password;
		this.realname = realname;
		this.sex = sex;
		this.mail = mail;
		this.city = city;
		this.headname = headname;
		this.signature = signature;
	}

	public int getUserid() {
		return userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRealname() {
		return realname;
	}

	public void setRealname(String realname) {
		this.realname = realname;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getHeadname() {
		return headname;
	}

	public void setHeadname(String headname) {
		this.headname = headname;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	@Override
	public String toString() {
		return "user [userid=" + userid + ", username=" + username + ", password=" + password + ", realname=" + realname
				+ ", sex=" + sex + ", mail=" + mail + ", city=" + city + ", headname=" + headname + ", signature="
				+ signature + "]";
	}
	
	
	
}
