package com.neuedu.Model.entity;

import java.util.Date;

public class Post {

	private int postid;
	private String title;
	private String content;
	private Date ptime;
	private int nice;
	private String ptype;
	private String state;
	private int seenum;
	private User user;
    private int commentnum;
	
    
    public Post() {
		super();
	}


	public Post(int postid, String title, String content, Date ptime, int nice, String ptype, String state, int seenum,
			User user, int commentnum) {
		super();
		this.postid = postid;
		this.title = title;
		this.content = content;
		this.ptime = ptime;
		this.nice = nice;
		this.ptype = ptype;
		this.state = state;
		this.seenum = seenum;
		this.user = user;
		this.commentnum = commentnum;
	}


	public int getPostid() {
		return postid;
	}


	public void setPostid(int postid) {
		this.postid = postid;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getContent() {
		return content;
	}


	public void setContent(String content) {
		this.content = content;
	}


	public Date getPtime() {
		return ptime;
	}


	public void setPtime(Date ptime) {
		this.ptime = ptime;
	}


	public int getNice() {
		return nice;
	}


	public void setNice(int nice) {
		this.nice = nice;
	}


	public String getPtype() {
		return ptype;
	}


	public void setPtype(String ptype) {
		this.ptype = ptype;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public int getSeenum() {
		return seenum;
	}


	public void setSeenum(int seenum) {
		this.seenum = seenum;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public int getCommentnum() {
		return commentnum;
	}


	public void setCommentnum(int commentnum) {
		this.commentnum = commentnum;
	}


	@Override
	public String toString() {
		return "Post [postid=" + postid + ", title=" + title + ", content=" + content + ", ptime=" + ptime + ", nice="
				+ nice + ", ptype=" + ptype + ", state=" + state + ", seenum=" + seenum + ", user=" + user
				+ ", commentnum=" + commentnum + "]";
	}
	
	
	
	
	
}
