package com.neuedu.Model.entity;

import java.util.Date;

public class Reply {

	private int rid;
	private String replycontent;
	private Date replytime;
	private Comment comment;
	
	
	public Reply() {
		super();
	}


	public Reply(int rid, String replycontent, Date replytime, Comment comment) {
		super();
		this.rid = rid;
		this.replycontent = replycontent;
		this.replytime = replytime;
		this.comment = comment;
	}


	public int getRid() {
		return rid;
	}


	public void setRid(int rid) {
		this.rid = rid;
	}


	public String getReplycontent() {
		return replycontent;
	}


	public void setReplycontent(String replycontent) {
		this.replycontent = replycontent;
	}


	public Date getReplytime() {
		return replytime;
	}


	public void setReplytime(Date replytime) {
		this.replytime = replytime;
	}


	public Comment getComment() {
		return comment;
	}


	public void setComment(Comment comment) {
		this.comment = comment;
	}


	@Override
	public String toString() {
		return "Reply [rid=" + rid + ", replycontent=" + replycontent + ", replytime=" + replytime + ", comment="
				+ comment + "]";
	}
	
	

	
	
}
