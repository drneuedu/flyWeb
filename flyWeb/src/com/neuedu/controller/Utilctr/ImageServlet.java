package com.neuedu.controller.Utilctr;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.neuedu.Util.ImageUtil;

/**
 * Servlet implementation class ImageServlet
 */
@WebServlet("/ImageServlet")
public class ImageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ImageServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 生成验证码图片
        Object[] objs = ImageUtil.createImage();
        // 将验证码存入session
        String imgcode = (String) objs[0];
        request.getSession().setAttribute("imgcode", imgcode);
        // 将图片输出给浏览器
        BufferedImage img = (BufferedImage) objs[1];
        response.setContentType("image/png");
        // tomcat自动创建输出流
        // 目标就是本次访问的浏览器
        OutputStream os = response.getOutputStream();
        ImageIO.write(img, "png", os);
        os.close();
	}
}
