package com.neuedu.controller.commonctr;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.neuedu.Model.entity.Post;
import com.neuedu.Model.service.mainlService;

/**
 * Servlet implementation class mainlServlet
 */
@WebServlet("/mainlServlet")
public class mainlServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public mainlServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获取文章类型
		String ptype=request.getParameter("ptype");
		System.out.println(ptype);
		//本周热议
		//调用service处理数据库，查询出文章标题，按照倒序拍序。
		mainlService ms=new mainlService();
		List<Post> listhot=new ArrayList<Post>();
		
		
		
		List<Post> list=new ArrayList<Post>();
		//对类型进行判断
		if("首页".equals(ptype)){
			//如果点击了首页返回所有文章
			list=ms.selectAllPost();
			System.out.println(list);
			listhot=ms.selectTitle();
			System.out.println(listhot);
			request.setAttribute("hotList", listhot);
			request.setAttribute("postList", list);
			request.getRequestDispatcher("/main.jsp").forward(request, response);
		}
		else{
			//点击专栏，按照专栏分类
			list=ms.selectPtypePost(ptype);
			request.setAttribute("postList", list);
			request.getRequestDispatcher("/main.jsp").forward(request, response);
		}
		
	}

}
